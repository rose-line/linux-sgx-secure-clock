/*
 * Copyright (C) 2011-2017 Intel Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of Intel Corporation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


#include <stdarg.h>
#include <stdio.h>      /* vsnprintf */

#include "Enclave.h"
#include "Enclave_t.h"  /* print_string */
#include "sgx_trts.h"
#include "sgx_tseal.h"
#include "sgx_tae_service.h"

#include "sgx_eid.h"
#include "EnclaveMessageExchange.h"
#include "error_codes.h"
#include "Utility_E.h"
#include "sgx_thread.h"
#include "sgx_dh.h"

//Function pointer table containing the list of functions that the enclave exposes
const struct {
    size_t num_funcs;
    const void* table[0];
} func_table = {
    0,
    {}
};

/* 
 * printf: 
 *   Invokes OCALL to display the enclave buffer to the terminal.
 */
void printf(const char *fmt, ...)
{
    char buf[BUFSIZ] = {'\0'};
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);
    ocall_print_string(buf);
}

void ecall_readclock_experiment()
{
	uint64_t clock;
	ocall_tpm_getclock(&clock);
	// printf("clock value: %llu\n", clock);
	// volatile uint64_t copy = *(uint64_t *) &clock;
}

void ecall_nullcall()
{
	asm volatile("movq %rax, %rax");
}

void ecall_nestocall()
{
	ocall_nullcall();
}

int ecall_pse_session_create_experiment()
{
    int busy_retry_times = 2;
    int i = 0;
    int ret = SGX_SUCCESS;

    sgx_time_source_nonce_t nonce = {0};
    sgx_time_t timestamp;
    volatile sgx_time_t vts;
    do {
        ret = sgx_create_pse_session();
    } while (ret == SGX_ERROR_BUSY && busy_retry_times--);

    if (ret != SGX_SUCCESS)
        return ret;

    ret = sgx_get_trusted_time(&timestamp, &nonce);
    vts = (volatile sgx_time_t) timestamp;
    sgx_close_pse_session();
}

int ecall_e2e_experiment(sgx_enclave_id_t src_enclave_id, sgx_enclave_id_t dest_enclave_id)
{
    ATTESTATION_STATUS ke_status = SUCCESS;
    dh_session_t dest_session_info;
    external_param_struct_t *p_struct_var, struct_var;
    internal_param_struct_t internal_struct_var;
    uint32_t target_fn_id, msg_type;
    char* marshalled_inp_buff;
    size_t marshalled_inp_buff_len;
    char* out_buff;
    size_t out_buff_len;
    size_t max_out_buff_size;
    char* retval;

    max_out_buff_size = 50;
    msg_type = ENCLAVE_TO_ENCLAVE_CALL;
    target_fn_id = 0;
    internal_struct_var.ivar1 = 0x5;
    internal_struct_var.ivar2 = 0x6;
    struct_var.var1 = 0x3;
    struct_var.var2 = 0x4;
    struct_var.p_internal_struct = &internal_struct_var;
    p_struct_var = &struct_var;

    size_t len_data = sizeof(struct_var) - sizeof(struct_var.p_internal_struct);
    size_t len_ptr_data = sizeof(internal_struct_var);

    ke_status = create_session(src_enclave_id, dest_enclave_id, &dest_session_info);

    if (ke_status != SUCCESS)
    {
        return ke_status;
    }

    ke_status = marshal_input_parameters_e1_foo1(target_fn_id, msg_type, p_struct_var, len_data,
                                len_ptr_data, &marshalled_inp_buff, &marshalled_inp_buff_len);

    
    if (ke_status != SUCCESS)
    {
        return ke_status;
    }

    ocall_mark_start();
    ke_status = send_request_receive_response(src_enclave_id, dest_enclave_id, &dest_session_info,
                  marshalled_inp_buff, marshalled_inp_buff_len, max_out_buff_size, &out_buff, &out_buff_len);
    ocall_mark_end();

    if (ke_status != SUCCESS)
    {
        return ke_status;
    }

    ke_status = unmarshal_retval_and_output_parameters_e1_foo1(out_buff, p_struct_var, &retval);

    return ke_status;
}

//Function that is used to verify the trust of the other enclave
//Each enclave can have its own way verifying the peer enclave identity
extern "C" uint32_t verify_peer_enclave_trust(sgx_dh_session_enclave_identity_t* peer_enclave_identity)
{
    if(!peer_enclave_identity)
    {
        return INVALID_PARAMETER_ERROR;
    }
    if(peer_enclave_identity->isv_prod_id != 0 || !(peer_enclave_identity->attributes.flags & SGX_FLAGS_INITTED))
        // || peer_enclave_identity->attributes.xfrm !=3)// || peer_enclave_identity->mr_signer != xx //TODO: To be hardcoded with values to check
    {
        return ENCLAVE_TRUST_ERROR;
    }
    else
    {
        return SUCCESS;
    }
}


//Dispatcher function that calls the approriate enclave function based on the function id
//Each enclave can have its own way of dispatching the calls from other enclave
extern "C" uint32_t enclave_to_enclave_call_dispatcher(char* decrypted_data,
                                                       size_t decrypted_data_length,
                                                       char** resp_buffer,
                                                       size_t* resp_length)
{
    ms_in_msg_exchange_t *ms;
    uint32_t (*fn1)(ms_in_msg_exchange_t *ms, size_t, char**, size_t*);
    if(!decrypted_data || !resp_length)
    {
        return INVALID_PARAMETER_ERROR;
    }
    ms = (ms_in_msg_exchange_t *)decrypted_data;
    if(ms->target_fn_id >= func_table.num_funcs)
    {
        return INVALID_PARAMETER_ERROR;
    }
    fn1 = (uint32_t (*)(ms_in_msg_exchange_t*, size_t, char**, size_t*))func_table.table[ms->target_fn_id];
    return fn1(ms, decrypted_data_length, resp_buffer, resp_length);
}

//Operates on the input secret and generates the output secret
uint32_t get_message_exchange_response(uint32_t inp_secret_data)
{
    uint32_t secret_response;

    //User should use more complex encryption method to protect their secret, below is just a simple example
    secret_response = inp_secret_data & 0x11111111; 

    return secret_response;

}

//Generates the response from the request message
extern "C" uint32_t message_exchange_response_generator(char* decrypted_data,
                                              char** resp_buffer,
                                              size_t* resp_length)
{
    ms_in_msg_exchange_t *ms;
    uint32_t inp_secret_data;
    uint32_t out_secret_data;
    if(!decrypted_data || !resp_length)
    {
        return INVALID_PARAMETER_ERROR;
    }
    ms = (ms_in_msg_exchange_t *)decrypted_data;

    if(umarshal_message_exchange_request(&inp_secret_data,ms) != SUCCESS)
        return ATTESTATION_ERROR;

    out_secret_data = get_message_exchange_response(inp_secret_data);

    if(marshal_message_exchange_response(resp_buffer, resp_length, out_secret_data) != SUCCESS)
        return MALLOC_ERROR;

    return SUCCESS;

}