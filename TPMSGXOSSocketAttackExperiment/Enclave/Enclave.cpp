/*
 * Copyright (C) 2011-2017 Intel Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of Intel Corporation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


#include <stdarg.h>
#include <stdio.h>      /* vsnprintf */

#include "Enclave.h"
#include "Enclave_t.h"  /* print_string */
#include "sgx_trts.h"
#include "sgx_tseal.h"
#include "sgx_tae_service.h"
/* 
 * printf: 
 *   Invokes OCALL to display the enclave buffer to the terminal.
 */
void printf(const char *fmt, ...)
{
    char buf[BUFSIZ] = {'\0'};
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);
    ocall_print_string(buf);
}

void ecall_readclock_experiment()
{
	uint64_t clock;

    int remain = 1000;
    while (remain > 0) {
        ocall_tpm_getclock(&clock);
        ocall_timestamp_and_report(0);
        remain--;
    }

	// printf("clock value: %llu\n", clock);
	// volatile uint64_t copy = *(uint64_t *) &clock;
}

void ecall_nullcall()
{
	asm volatile("movq %rax, %rax");
}

void ecall_nestocall()
{
	ocall_nullcall();
}

int ecall_pse_session_create_experiment(uint64_t *ts)
{
    int busy_retry_times = 2;
    int i = 0;
    int ret = SGX_SUCCESS;

    sgx_time_source_nonce_t nonce = {0};
    sgx_time_t timestamp;
    do {
        ret = sgx_create_pse_session();
    } while (ret == SGX_ERROR_BUSY && busy_retry_times--);

    if (ret != SGX_SUCCESS)
        return ret;

    ret = sgx_get_trusted_time(&timestamp, &nonce);
    *ts = timestamp;

    sgx_time_t old_timestamp = timestamp;

    int remain = 100;
    while (remain > 0) {
        sgx_get_trusted_time(&timestamp, &nonce);
        if (timestamp - old_timestamp >= 1) {
            old_timestamp = timestamp;
            ocall_timestamp_and_report(timestamp);
            remain--;
        }
    }
    sgx_close_pse_session();
}