import socket
import sys
import os
import struct
# Create a UDS socket
sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
os.unlink('/tmp/timeclient')
sock.bind('/tmp/timeclient')

# Connect the socket to the port where the server is listening
server_address = '/tmp/timesock'
print('connecting to {}'.format(server_address))
try:
    sock.connect(server_address)
except socket.error as msg:
    print(msg)
    sys.exit(1)

try:

    # Send data
    message = b'time'
    print('sending {!r}'.format(message))
    sock.send(message)

    amount_received = 0
    amount_expected = 24

    while amount_received < amount_expected:
        data = sock.recv(24)
        amount_received += len(data)
        print(struct.unpack('QQQ', data))

finally:
    print('closing socket')
    sock.close()