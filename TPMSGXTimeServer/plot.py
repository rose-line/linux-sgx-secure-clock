import matplotlib.pyplot as plt
import numpy as np
import subprocess as sp
import time
import multiprocessing as mp

timestamps_py = []
timestamps_app = []
timestamps_attacked = []

def launch_and_timestamp(attacked, ts):
	if attacked:
		completed = sp.run("./app_attacked", shell=True, stdout=sp.PIPE)
	else:
		completed = sp.run("./app", shell=True, stdout=sp.PIPE)

	ts.value = int(completed.stdout.decode("utf-8"))

	return 0

for i in range(20):
	print("iteration" + " " + str(i))
	ts = mp.Value('i', 0)
	ts_attacked = mp.Value('i', 0)

	p = mp.Process(target=launch_and_timestamp, args=(False, ts))
	p_attacked = mp.Process(target=launch_and_timestamp, args=(True, ts_attacked))

	timestamps_py.append(int(time.time()))
	
	p.start()
	p_attacked.start()

	p.join()
	p_attacked.join()

	timestamps_app.append(ts.value)
	timestamps_attacked.append(ts_attacked.value)

print(timestamps_py)
print(timestamps_app)
print(timestamps_attacked)

# plt.plot(timestamps_py, timestamps_app, 'b-', label='Timestamping from SGX Attacked')
# plt.plot(timestamps_py, timestamps_attacked, 'r-', label='Timestamping from SGX Unattacked
plt.plot(timestamps_app, timestamps_app, 'g--', label="Unattacked Timestamp")
plt.plot(timestamps_app, timestamps_attacked, 'r-', label="Attacked Timestamp")
plt.legend()
plt.show()


