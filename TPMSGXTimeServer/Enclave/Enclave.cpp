/*
 * Copyright (C) 2011-2017 Intel Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of Intel Corporation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


#include <stdarg.h>
#include <stdio.h>      /* vsnprintf */
#include <queue>

#include "Enclave.h"
#include "Enclave_t.h"  /* print_string */
#include "sgx_trts.h"
#include "sgx_tseal.h"
#include "sgx_tae_service.h"

using namespace std;

#define WINDOW 16
/* 
 * printf: 
 *   Invokes OCALL to display the enclave buffer to the terminal.
 */
void printf(const char *fmt, ...)
{
    char buf[BUFSIZ] = {'\0'};
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);
    ocall_print_string(buf);
}

void ecall_readclock_experiment()
{
	uint64_t clock;
	ocall_tpm_getclock(&clock);
	// printf("clock value: %llu\n", clock);
	// volatile uint64_t copy = *(uint64_t *) &clock;
}

void ecall_nullcall()
{
	asm volatile("movq %rax, %rax");
}

void ecall_nestocall()
{
	ocall_nullcall();
}

int ecall_pse_session_create_experiment(uint64_t *ts)
{
    int busy_retry_times = 2;
    int i = 0;
    int ret = SGX_SUCCESS;

    sgx_time_source_nonce_t nonce = {0};
    sgx_time_t timestamp;

    do {
        ret = sgx_create_pse_session();
    } while (ret == SGX_ERROR_BUSY && busy_retry_times--);

    if (ret != SGX_SUCCESS)
        return ret;

    ret = sgx_get_trusted_time(&timestamp, &nonce);
    *ts = timestamp;
    sgx_close_pse_session();
}

int ecall_subtick_same_process_experiment(uint64_t *ts, int *flag, uint64_t* tick, uint64_t *tps, uint64_t *error)
{
    int busy_retry_times = 2;
    int i = 0;
    int ret = SGX_SUCCESS;

    sgx_time_source_nonce_t nonce = {0};
    sgx_time_t timestamp = 0;
    sgx_time_t old_ts = 0;

    do {
        ret = sgx_create_pse_session();
    } while (ret == SGX_ERROR_BUSY && busy_retry_times--);

    if (ret != SGX_SUCCESS)
        return ret;

    double avg = 0;
    queue<uint64_t>  tick_queue;


    while (true) {
        ret = sgx_get_trusted_time(&timestamp, &nonce);
        *ts = timestamp;
        if (timestamp > old_ts) {
            uint64_t tick_copy = *tick;

            *flag = !*flag;
            asm volatile("" ::: "memory");
            old_ts = timestamp;

            if (tick_queue.size() == WINDOW) {
                avg = avg + (double) tick_copy / WINDOW - (double) tick_queue.front() / WINDOW;
                tick_queue.pop();
            } else {
                avg = (avg * tick_queue.size() + (double) tick_copy) / (tick_queue.size() + 1);
            }

            *tps = (uint64_t) (avg + 0.5);


            uint64_t min = 0xFFFFFFFFFFFFFFFF;
            uint64_t max = 0x0;

            queue<uint64_t> tick_queue_copy = tick_queue;
            while (!tick_queue_copy.empty()) {
                uint64_t curr = tick_queue_copy.front();

                if (curr > max) {
                    max = curr;
                }

                if (curr < min) {
                    min = curr;
                }

                tick_queue_copy.pop();
            }
            
            *error = (max - min + 1) / 2;

            tick_queue.push(tick_copy);

            printf("%s %p\n", "ts pointer in enclave", ts);
            printf("%llu %s\n", *ts, "second");
            printf("%llu %s\n", *tps, "ticks");
            printf("%llu %s\n", tick_copy, "tps");
            printf("%llu %s\n", *error, "error");
            continue;
        }
    }
    
    
    sgx_close_pse_session();
}

void ecall_subtick(int *flag, uint64_t *tick) 
{
    int old_flag = *flag;
    while (true) {
        

        if (old_flag != *(volatile int *)flag) {
            *tick = 0;
            old_flag = *(volatile int *)flag;
        } else {
            // printf("%s\n", "else");
            *tick = *tick + 1;
        }
    }
}