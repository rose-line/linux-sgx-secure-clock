import matplotlib.pyplot as plt
import numpy as np

np.random.seed(1234)
x = [500, 500, 500, 1000, 1000, 1000, 1500, 1500, 1500, 2000, 2000, 2000]
y = [499, 501, 502, 998, 1001, 1002, 1501, 1502, 1500, 1998, 2001, 2003]

plt.plot(x, y, '.')
plt.show()