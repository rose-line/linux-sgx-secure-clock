import socket
import sys
import os
import struct
import time
import threading
import numpy as np
import matplotlib.pyplot as plt
import json
import csv

def init():
    # Create a UDS socket
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_DGRAM)
    
    try:
        os.unlink('/tmp/timeclient')
    finally:
        sock.bind('/tmp/timeclient')

    # Connect the socket to the port where the server is listening
    server_address = '/tmp/timesock'
    print('connecting to {}'.format(server_address))
    try:
        sock.connect(server_address)
    except socket.error as msg:
        print(msg)
        sys.exit(1)

    return sock

def getTimeTuple(sock):
    # Send data
    message = b'time'
    # print('sending {!r}'.format(message))
    sock.send(message)

    amount_received = 0
    amount_expected = 24

    while amount_received < amount_expected:
        data = sock.recv(24)
        amount_received += len(data)
        return struct.unpack('QQQ', data)

def shutdown(sock):
    print('closing socket')
    sock.close()

def collect_data():
    sock = init()

    durations = [1]

    for i in range(150):
        duration = (i + 1) * 0.02
        for _ in range(50):
            durations.append(duration)

    print(durations)

    timebase = time.clock_gettime(time.CLOCK_MONOTONIC)

    wakeups = [timebase]
    responses = []
    responses_onlysgx = []

    for duration in durations:
        wakeups.append(wakeups[len(wakeups) - 1] + duration)
    wakeups = wakeups[1:]

    for ts in wakeups:
        while (time.clock_gettime(time.CLOCK_MONOTONIC) < ts):
            pass
        sec, nsec, err = getTimeTuple(sock)
        responses.append(sec + float(nsec) * 1e-9)
        responses_onlysgx.append(float(sec))

    print(wakeups)
    print(responses)

    wakeups_durations = [x - y for x, y in zip(wakeups[1:], wakeups[:-1])]
    responses_durations = [x - y for x, y in zip(responses[1:], responses[:-1])]
    responses_onlysgx_durations = [x - y for x, y in zip(responses_onlysgx[1:], responses_onlysgx[:-1])]

    print(wakeups_durations)
    print(responses_durations)

    return wakeups, responses, responses_onlysgx, wakeups_durations, responses_durations, responses_onlysgx_durations

def dump_data_to_file(results_tuple, path):
    data_file = open(path, 'w')
    json.dump(list(results_tuple), data_file)
    
def plot_precision_graph(wakeups_durations, responses_durations, responses_onlysgx_durations):
    plt.plot(wakeups_durations, responses_durations, "b.")
    plt.plot(wakeups_durations, responses_onlysgx_durations, "g.")
    plt.show()

def read_data_from_file(path):
    data_file = open(path, 'r')
    return json.load(data_file)

def plot_var_load_graph():
    data_quiet = read_data_from_file('data/service_comp_100t.json')
    data_100t = read_data_from_file('data/service_comp_1t.json')

    # Extract useful columns from raw data
    wakeups_durations = np.array(data_quiet[3]).round(decimals=9)
    responses_durations_quiet = np.array(data_quiet[4]).round(decimals=9)
    responses_durations_100t = np.array(data_100t[4]).round(decimals=9)
    responses_durations_onlysgx_quiet = np.array(data_quiet[5]).round(decimals=9)
    responses_durations_onlysgx_100t = np.array(data_100t[5]).round(decimals=9)

    # De-duplicate the ground truth durations in raw data
    gt_durations_raw = np.array(data_quiet[3]).round(decimals=9)
    gt_durations = np.unique(gt_durations_raw)

    # Compute variance for each duration for each load
    grouped_durations_quiet = [np.array([y[1] for y in filter((lambda x: gt_durations_raw[x[0]] == duration), enumerate(responses_durations_quiet))]) for duration in gt_durations]
    stds_quiet = [np.std(array) for array in grouped_durations_quiet]
    grouped_durations_100t = [np.array([y[1] for y in filter((lambda x: gt_durations_raw[x[0]] == duration), enumerate(responses_durations_100t))]) for duration in gt_durations]
    stds_100t = [np.std(array) for array in grouped_durations_100t]
    print(stds_quiet)
    print(stds_100t)

    print(np.average(stds_quiet))
    print(np.average(stds_100t))

    plt.title("Stress Threads = 100, Average STDDEV = 16.0ms")
    plt.plot(wakeups_durations, responses_durations_quiet, "b.")
    plt.plot(wakeups_durations, responses_durations_onlysgx_quiet, "g.")
    plt.show()

def convert_json_to_313_csv(file_path):
    data_py = read_data_from_file(file_path)

    system_timestamps = np.array(data_py[0]).round(decimals=9)
    sgx_timestamps = np.array(data_py[2]).round(decimals=9)
    subticks = ((np.array(data_py[1]).round(decimals=9) - sgx_timestamps) * 1e9).astype(int)
    sgx_timestamps = sgx_timestamps.astype(int)

    with open(file_path + '.csv', 'w') as csvfile:
        writer = csv.writer(csvfile, delimiter=',')
        writer.writerow(['System', 'SGX', 'Subtick'])
        for i in range(system_timestamps.shape[0]):
            writer.writerow([system_timestamps[i], sgx_timestamps[i], subticks[i]])



def main():
    path = 'data/service_comp_quiet.json'
    # results = collect_data()
    # dump_data_to_file(results, path)
    # plot_precision_graph(results[3], results[4], results[5])
    # plot_var_load_graph()
    # print(getTimeTuple(init()))
    convert_json_to_313_csv(path)

if __name__ == '__main__':
    main()