/*
 * Copyright (C) 2011-2017 Intel Corporation. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 *   * Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in
 *     the documentation and/or other materials provided with the
 *     distribution.
 *   * Neither the name of Intel Corporation nor the names of its
 *     contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */


#include <stdarg.h>
#include <stdio.h>      /* vsnprintf */
#include <queue>
#include <cmath>

#include "Enclave.h"
#include "Enclave_t.h"  /* print_string */
#include "sgx_trts.h"
#include "sgx_tseal.h"
#include "sgx_tae_service.h"
#include "sgx_thread.h"

using namespace std;

static sgx_thread_mutex_t global_mutex = SGX_THREAD_MUTEX_INITIALIZER;
static int p1 = 16;
static int p2 = 16;
static int p3 = 16;
static int num = 24;

#define WINDOW 16
#define REPLAY_PROTECTED_SECRET_SIZE  32
/* 
 * printf: 
 *   Invokes OCALL to display the enclave buffer to the terminal.
 */
void printf(const char *fmt, ...)
{
    char buf[BUFSIZ] = {'\0'};
    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);
    ocall_print_string(buf);
}

/*
void ecall_readclock_experiment()
{
	uint64_t clock;
	ocall_tpm_getclock(&clock);
	// printf("clock value: %llu\n", clock);
	// volatile uint64_t copy = *(uint64_t *) &clock;
}
*/
void ecall_nullcall()
{
	asm volatile("movq %rax, %rax");
}

void ecall_nestocall()
{
	ocall_nullcall();
}

int ecall_pse_session_create_experiment(uint64_t *ts)
{
    int busy_retry_times = 2;
    int i = 0;
    int ret = SGX_SUCCESS;

    sgx_time_source_nonce_t nonce = {0};
    sgx_time_t timestamp;

    do {
        ret = sgx_create_pse_session();
    } while (ret == SGX_ERROR_BUSY && busy_retry_times--);

    if (ret != SGX_SUCCESS)
        return ret;

    ret = sgx_get_trusted_time(&timestamp, &nonce);
    *ts = timestamp;
    sgx_close_pse_session();
}

int ecall_subtick_same_process_experiment(uint64_t *ts, int *flag_t, int *flag, uint64_t* tick1, uint64_t* tick2, uint64_t* tick3, uint64_t* tick4, uint64_t* tick5, uint64_t *tps, uint64_t *error, uint64_t *last_second_tick)
{
    int busy_retry_times = 2;
    int i = 0;
    int ret = SGX_SUCCESS;

    sgx_time_source_nonce_t nonce = {0};
    sgx_time_t timestamp = 0;
    sgx_time_t old_ts = 0;
    uint8_t secret;
    uint8_t m_secret;

    do {
        ret = sgx_create_pse_session();
    } while (ret == SGX_ERROR_BUSY && busy_retry_times--);

    if (ret != SGX_SUCCESS)
        return ret;

    //double avg = 0;
    //double variance = 0;
    //double stddev = 0;
    //queue<uint64_t>  tick_queue;


    while (true) {
        ret = sgx_get_trusted_time(&timestamp, &nonce);
        //*ts = timestamp;
        if (timestamp > old_ts) {
            uint64_t tick_copy = *tick1 + *tick2 + *tick3 + *tick4 + *tick5;

            ret = sgx_read_rand(&secret, sizeof(secret));
            if(ret != SGX_SUCCESS)
                break;
            //printf("%u\n", secret);
            
            // order is important: avoid race condition
            sgx_thread_mutex_lock(&global_mutex);
            *last_second_tick = tick_copy;
            *ts = timestamp; //fatima
            //*flag_t = (int) (secret / 86); // 0 to 2
            *flag_t = (int) (secret / 52); // 0 to 4
            int tmp_flag = *flag;
            *flag = !tmp_flag;
            // *flag = !*flag;
            asm volatile("" ::: "memory");
            //*flag_t = secret >> 7; // 0 to 1           
            
            //printf("%u\n", *flag_t);            
            old_ts = timestamp;
            //sgx_thread_mutex_unlock(&global_mutex);

/*            if (tick_queue.size() == WINDOW) {
                double oldavg = avg;
                double oldts = (double) tick_queue.front();
                double newts = (double) tick_copy;

                avg = avg + (newts - oldts) / WINDOW;
                variance += (newts - oldts) * (newts - avg + oldts - oldavg) / (WINDOW - 1);
                stddev = sqrt(variance);
                tick_queue.pop();
            } else {
                double oldavg = avg;
                double newts = (double) tick_copy;

                avg = (avg * tick_queue.size() + (double) tick_copy) / (tick_queue.size() + 1);
                if (tick_queue.size() > 0) {
                    double k = (double) tick_queue.size();
                    variance = ((k - 1) * variance + (newts - avg) * (newts - oldavg)) / k;
                    stddev = sqrt(variance);
                }
            }

            *tps = (uint64_t) (avg + 0.5);

            uint64_t min = 0xFFFFFFFFFFFFFFFF;
            uint64_t max = 0x0;

            queue<uint64_t> tick_queue_copy = tick_queue;
            while (!tick_queue_copy.empty()) {
                uint64_t curr = tick_queue_copy.front();

                if (curr > max) {
                    max = curr;
                }

                if (curr < min) {
                    min = curr;
                }

                tick_queue_copy.pop();
            }
            
            // *error = (max - min + 1) / 2;
            *error = (uint64_t) (3 * stddev) + 0.5;
            tick_queue.push(tick_copy);
*/
            *tps = 0;   //fatima
            *error = 0; //fatima
            sgx_thread_mutex_unlock(&global_mutex);
            // printf("%s %p\n", "ts pointer in enclave", ts);
            // printf("%llu %s\n", *ts, "second");
            // printf("%llu %s\n", tick_copy, "ticks");
            // printf("%llu %s\n", *tps, "tps");
            // printf("%llu %s\n", *error, "error");
            //continue;
        }
    }
        
    sgx_close_pse_session();
}
/*
void ecall_subtick(int *flag, uint64_t *tick, int *flag_100t) 
{
    int old_flag = *flag;
    while (true) {
        

        if (old_flag != *(volatile int *)flag) {
            *tick = 0;
            old_flag = *(volatile int *)flag;
        } else {
            // printf("%s\n", "else");
            *tick = *tick + 1;
        }

        if ((*tick & 0xFFFF) == 0) {
            *flag_100t = !(*flag_100t);
        }
    }
}
*/
//Fatima: enable multithread counting
void ecall_subtick1(int *flag_t, int *flag, uint64_t *tick, int *flag_100t) 
{
    int old_flag = *flag;
    //int num = 20;

    while (true) {
        
        if (old_flag != *(volatile int *)flag) {
            sgx_thread_mutex_lock(&global_mutex);
            *tick = 0;
        /*    if (*flag_t == 0){
                p1 = 23;
                p2 = 22;
                p3 = 21;
                //num = 21;
            }
        */
            old_flag = *(volatile int *)flag;
            sgx_thread_mutex_unlock(&global_mutex);
            //continue;
        }

        else if(*flag_t == 0){
        //else{
            //uint64_t tmp = *tick;
            //*tick = ++tmp;
            *tick = *tick + 1;
        
            if ((*tick & 0xFFFF) == 0) {
                *flag_100t = !(*flag_100t);
            }

        //}
            if ((*tick & ~(0xFFFF << 22)) == 0) {
                //sgx_thread_mutex_lock(&global_mutex);
                *flag_t = 1;
                //sgx_thread_mutex_unlock(&global_mutex);
            }

        }
    }
}

void ecall_subtick2(int *flag_t, int *flag, uint64_t *tick2, int *flag2_100t) 
{
    int old_flag = *flag;
    //int num = 20;

    while (true) {
        
        if (old_flag != *(volatile int *)flag) {
            //printf("%lu\n", *tick2);
            sgx_thread_mutex_lock(&global_mutex);
            *tick2 = 0;
        /*    if (*flag_t == 1){
                p1 = 21;
                p2 = 23;
                p3 = 22;
                //num = 22;
            } 
        */   
            old_flag = *(volatile int *)flag;
            sgx_thread_mutex_unlock(&global_mutex);
            //continue;
        }

        else if(*flag_t == 1){        
        //else{
            *tick2 = *tick2 + 1;
        
            if ((*tick2 & 0xFFFF) == 0) {
                *flag2_100t = !(*flag2_100t);
            }
       
        //}
            if ((*tick2 & ~(0xFFFF << 22)) == 0) {
                //printf("%lu\n", *tick2);
                //sgx_thread_mutex_lock(&global_mutex);
                *flag_t = 2;
                //sgx_thread_mutex_unlock(&global_mutex);
            }

       }
    }
}

void ecall_subtick3(int *flag_t, int *flag, uint64_t *tick3, int *flag3_100t) 
{
    int old_flag = *flag;
    //int num = 20;

    while (true) {
        
        if (old_flag != *(volatile int *)flag) {
            sgx_thread_mutex_lock(&global_mutex);
            *tick3 = 0;
        /*
            if (*flag_t == 2){
                p1 = 22;
                p2 = 21;
                p3 = 23;
                //num = 23;
            }
        */    
            old_flag = *(volatile int *)flag;
            sgx_thread_mutex_unlock(&global_mutex);
            //continue;
        }

        else if(*flag_t == 2){        
        //else{
            *tick3 = *tick3 + 1;
            
            if ((*tick3 & 0xFFFF) == 0) {
                *flag3_100t = !(*flag3_100t);
            }
        
        //}
            if ((*tick3 & ~(0xFFFF << 22)) == 0) {
                //sgx_thread_mutex_lock(&global_mutex);
                //*flag_t == 3;
                *flag_t = 3;
                //sgx_thread_mutex_unlock(&global_mutex);
            }

       }
    }
}

void ecall_subtick4(int *flag_t, int *flag, uint64_t *tick4, int *flag4_100t) 
{
    int old_flag = *flag;
    //int num = 20;

    while (true) {
        
        if (old_flag != *(volatile int *)flag) {
            sgx_thread_mutex_lock(&global_mutex);
            *tick4 = 0;
        /*
            if (*flag_t == 2){
                p1 = 22;
                p2 = 21;
                p3 = 23;
                //num = 23;
            }
        */    
            old_flag = *(volatile int *)flag;
            sgx_thread_mutex_unlock(&global_mutex);
            //continue;
        }

        else if(*flag_t == 3){        
        //else{
            *tick4 = *tick4 + 1;
            
            if ((*tick4 & 0xFFFF) == 0) {
                *flag4_100t = !(*flag4_100t);
            }
        
        //}
            if ((*tick4 & ~(0xFFFF << 22)) == 0) {
                //sgx_thread_mutex_lock(&global_mutex);
                //*flag_t == 3;
                *flag_t = 4;
                //sgx_thread_mutex_unlock(&global_mutex);
            }

       }
    }
}

void ecall_subtick5(int *flag_t, int *flag, uint64_t *tick5, int *flag5_100t) 
{
        int old_flag = *flag;
    //int num = 20;

    while (true) {
        
        if (old_flag != *(volatile int *)flag) {
            sgx_thread_mutex_lock(&global_mutex);
            *tick5 = 0;
        /*
            if (*flag_t == 2){
                p1 = 22;
                p2 = 21;
                p3 = 23;
                //num = 23;
            }
        */    
            old_flag = *(volatile int *)flag;
            sgx_thread_mutex_unlock(&global_mutex);
            //continue;
        }

        else if(*flag_t == 4){        
        //else{
            *tick5 = *tick5 + 1;
            
            if ((*tick5 & 0xFFFF) == 0) {
                *flag5_100t = !(*flag5_100t);
            }
        
        //}
            if ((*tick5 & ~(0xFFFF << 22)) == 0) {
                //sgx_thread_mutex_lock(&global_mutex);
                //*flag_t == 3;
                *flag_t = 0;
                //sgx_thread_mutex_unlock(&global_mutex);
            }

       }
    }
}

// Threads counting within one second
/*
void ecall_subtick1(int *t_flag1, int *t_flag2, int *flag, uint64_t *tick, int *flag_100t) 
{
    int old_flag = *flag;
    int old_tflag1 = *t_flag1;
    int old_tflag2 = *t_flag2;

    while (true) {
        
        if (old_flag != *(volatile int *)flag) {
            sgx_thread_mutex_lock(&global_mutex);
            *tick = 0;
            old_flag = *(volatile int *)flag;
            sgx_thread_mutex_unlock(&global_mutex);
            continue;
        }
     
        if((old_tflag1 == *(volatile int *)t_flag1) && (old_tflag2 == *(volatile int *)t_flag2)){
        //if(old_tflag != *(volatile int *)t_flag){ //Fatima: Only count when its your turn
        //else{
            *tick = *tick + 1;
//        
//            if ((*tick & 0xFFFF) == 0) {
//                *flag_100t = !(*flag_100t);
//            }

        //}
            if ((*tick & 0x7FFFFFFF) == 0) {
                sgx_thread_mutex_lock(&global_mutex);
                *t_flag1 = !(*t_flag1);
                sgx_thread_mutex_unlock(&global_mutex);
            }
        }
    }
}

void ecall_subtick2(int *t_flag1, int *t_flag2, int *flag, uint64_t *tick2, int *flag2_100t) 
{
    int old_flag = *flag;
    int old_tflag1 = *t_flag1;
    int old_tflag2 = *t_flag2;
    while (true) {
        
        if (old_flag != *(volatile int *)flag) {
            sgx_thread_mutex_lock(&global_mutex);
            *tick2 = 0;
            old_flag = *(volatile int *)flag;
            sgx_thread_mutex_unlock(&global_mutex);
            //continue;
        }

        if((old_tflag1 != *(volatile int *)t_flag1) && (old_tflag2 == *(volatile int *)t_flag2)){ //Fatima: Only count when its your turn
        //else{
            *tick2 = *tick2 + 1;
//        
//            if ((*tick2 & 0xFFFF) == 0) {
//                *flag2_100t = !(*flag2_100t);
//            }
        
        //}
           if ((*tick2 & 0x7FFFFFFF) == 0) {
            sgx_thread_mutex_lock(&global_mutex);
              *t_flag2 = !(*t_flag2); // Comment for 2 threads
              //*t_flag1 = !(*t_flag1); // Added for 2 threads
              sgx_thread_mutex_unlock(&global_mutex);
            }
       }
    }
}

void ecall_subtick3(int *t_flag1, int *t_flag2, int *flag, uint64_t *tick3, int *flag3_100t) 
{
    int old_flag = *flag;
    int old_tflag1 = *t_flag1;
    int old_tflag2 = *t_flag2;
    while (true) {
        
        if (old_flag != *(volatile int *)flag) {
            sgx_thread_mutex_lock(&global_mutex);
            *tick3 = 0;
            old_flag = *(volatile int *)flag;
            sgx_thread_mutex_unlock(&global_mutex);
            //continue;
        }

        if((old_tflag1 != *(volatile int *)t_flag1) && (old_tflag2 != *(volatile int *)t_flag2)){ //Fatima: Only count when its your turn
        //else{
            *tick3 = *tick3 + 1;
//            
//            if ((*tick3 & 0xFFFF) == 0) {
//                *flag3_100t = !(*flag3_100t);
//            }
        
        //}
            if ((*tick3 & 0x7FFFFFFF) == 0) {
                sgx_thread_mutex_lock(&global_mutex);
                *t_flag1 = !(*t_flag1);
                *t_flag2 = !(*t_flag2); // Uncomment for 3 threads
                sgx_thread_mutex_unlock(&global_mutex);
            }
       }
    }
}

void ecall_subtick4(int *t_flag1, int *t_flag2, int *flag, uint64_t *tick4, int *flag4_100t) 
{
    int old_flag = *flag;
    int old_tflag1 = *t_flag1;
    int old_tflag2 = *t_flag2;
    while (true) {
        
        if (old_flag != *(volatile int *)flag) {
            sgx_thread_mutex_lock(&global_mutex);
            *tick4 = 0;
            old_flag = *(volatile int *)flag;
            sgx_thread_mutex_unlock(&global_mutex);
            //continue;
        }

        if((old_tflag1 == *(volatile int *)t_flag1) && (old_tflag2 != *(volatile int *)t_flag2)){ //Fatima: Only count when its your turn
        //else{
            *tick4 = *tick4 + 1;
//            
//            if ((*tick4 & 0xFFFF) == 0) {
//                *flag4_100t = !(*flag4_100t);
//            }
        
        //}
            if ((*tick4 & 0x7FFFFFFF) == 0) {
                sgx_thread_mutex_lock(&global_mutex);
                *t_flag2 = !(*t_flag2);
                sgx_thread_mutex_unlock(&global_mutex);
            }
       }
    }
}
*/